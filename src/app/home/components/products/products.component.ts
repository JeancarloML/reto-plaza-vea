import { Component, Input, OnInit } from '@angular/core';
import { ProductsService } from '../../../core/services/products/products.service';
import { Observable } from 'rxjs';
import { Product } from 'src/app/core/models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  @Input() products: Product[];
  constructor() {}
  ngOnInit(): void {}
}
