import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../core/services/products/products.service';
import { Product } from '../../../core/models/product.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  categories: String[];
  products: Product[];

  constructor(private productService: ProductsService) {
    this.loadCategories();
    this.loadProducts();
  }

  ngOnInit(): void {}

  loadCategories() {
    this.productService.getCategories().subscribe((res: []) => {
      this.categories = res;
    });
  }
  loadProductsOnCategory(categorie) {
    this.productService
      .getProductsOnCategorie(categorie)
      .subscribe((res: []) => {
        this.products = res;
      });
  }
  loadProducts() {
    this.productService.getProducts().subscribe((res: []) => {
      this.products = res;
    });
  }
}
