import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private httpClient: HttpClient) {}

  getProducts() {
    return this.httpClient.get(`${environment.url_api}/products`);
  }
  getCategories() {
    return this.httpClient.get(`${environment.url_api}/products/categories`);
  }
  getProductsOnCategorie(categorie) {
    return this.httpClient.get(`${environment.url_api}/products/category/${categorie}`);
  }
}
