import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  login(dni: string) {
    window.localStorage.setItem('dni', JSON.stringify({ dni }));
  }
  logout() {
    window.localStorage.removeItem('dni');
  }

  checkAuth() {
    if (window.localStorage.getItem('dni')) {
      return true;
    } else {
      return false;
    }
  }
}
